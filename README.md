# Shopping App- Client side- ready for evaluation

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

# Features

There are 3 pages-
### -http://localhost:3000/index-page
This page displayes the list of available products and allows the user to place orders. 
Multiple requests are made to the api when the page is loaded- the GET request to get all the products from the api. The DELETE request which deletes all empty products from the product table to ensure no products that are unavailable are displayed.
![alt text](Screenshot__442_.png)

### -http://localhost:3000/admin
This page allows the admin to add products
![alt text](Screenshot__445_.png)
### -http://localhost:3000/orders
This page displayes the orders and their details
![alt text](Screenshot__444_.png)
