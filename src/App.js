import logo from './logo.svg';
import './App.css';
import Index from './routes/Index'
import Orders from './routes/Orders'
import Admin from './routes/Admin'
import { Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div>
      <Routes>
        <Route path="/index-page" element={<Index/>}/>
        <Route path="/admin" element={<Admin/>}/>
        <Route path="/orders" element={<Orders/>}/>
      </Routes>
    </div>
    
  );
}

export default App;
