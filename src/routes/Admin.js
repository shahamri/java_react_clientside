import Axios from "axios"
import { useEffect, useState } from 'react';
function Admin() {
  
  
  const [submitObject, setSubmitObject] = useState([])
  const [name, setName] = useState("")
  const [price, setPrice] = useState("")
  const [quantity, setQuantity] = useState("")

  function onNameChange(e) {
    setName(e.target.value)
}
function onQuantityChange(e) {
    setQuantity(e.target.value)
}
function onPriceChange(e) {
    setPrice(e.target.value)
}

function onSubmit() {
  
  
 
  setSubmitObject(
      {
          name: name, 
          availableQuantity: quantity, 
          price: price
        
      }
  )

 

  Axios.post('http://localhost:8080/api/product/add', submitObject)
  
}

        return (
          <>
          <div>Add Product</div>
                    <div>Product Name</div>
                    <div><input onChange={onNameChange}/></div>
                    <div>Product Quantity</div>
                    <div><input  onChange={onQuantityChange}/></div>
                    <div>Product Price</div>
                    <div><input onChange={onPriceChange}/></div>

<br/>
<br/>
                    <div><button onClick={onSubmit}>Submit</button></div>
                
<br/>
<br/>
<br/>

          
                    </>
        );
}

export default Admin;