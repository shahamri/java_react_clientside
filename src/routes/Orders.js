import styles from './styles.module.css'
import Axios from "axios"
import { useEffect, useState } from 'react';

function Orders() {


    const [tableData, setTableData] = useState([])
    const [tableData1, setTableData1] = useState([])
    
    useEffect(() => {

        Axios.get('http://localhost:8080/api/order/getAll').then((res) => {
            setTableData(res.data)
        })
        Axios.get('http://localhost:8080/api/cart/getAll').then((res) => {
        setTableData1(res.data)
        })
    }, [] )



   

    return (
        <>
        
        <table className={styles.MainTable}>
                <tr>
                    <th>Order Id</th>
                    <th>Order Description</th>
                    <th>Customer ID</th>
                    <th>Customer Name</th>
                    <th>Customer Email</th>

                   
                </tr>
                
                    {tableData.map((rowData, index) => {
                        return(
                           // <>
                            <tr>
                                <td>{rowData.id}</td>
                                <td>{rowData.orderDescription}</td>
                               <td>{rowData.customer.id}</td>
                                <td>{rowData.customer.name}</td>
                                <td>{rowData.customer.email}</td>
                              
                            </tr>
                            
                            //</>
                            )
                            
                    }
                    
                    )
                    
                    }


                 
                    </table>

                    <table>
                    <tr>
                        <th>Product Id</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                 

                    
                    </tr>

                        {tableData1.map((rowData, index) => {
                            return(
                            // <>
                                <tr>
                                    <td>{rowData.productId}</td>
                                    <td>{rowData.productName}</td>
                                    <td>{rowData.quantity}</td>
                                    <td>{rowData.amount}</td>
                                 
                                
                                </tr>
                                
                                //</>
                                )
                                
                        }
                        
                        )
                        
                        }


                    
                        </table>

           

       
        </>
    );
        
    
    
            
}

export default Orders;
