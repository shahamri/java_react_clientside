import styles from './styles.module.css'
import Axios from "axios"
import { useEffect, useState } from 'react';

function Index() {


    const [tableData, setTableData] = useState([])
    const [submitObject, setSubmitObject] = useState([])
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [orderDescription, setOrderDescription] = useState("")

    useEffect(() => {
        Axios.delete('http://localhost:8080/api/product/deleteEmpty')
        Axios.get('http://localhost:8080/api/product/getAll').then((res) => {
            setTableData(res.data)
        })
    }, [] )

    function onNameChange(e) {
        setName(e.target.value)
    }
    function onEmailChange(e) {
        setEmail(e.target.value)
    }
    function onDescriptionChange(e) {
        setOrderDescription(e.target.value)
    }

    function onTypeNumber(e, index) {
        console.log(index   ,"this is the index")
        console.log(e.target.value   ,"this is the e value")
        let tempTableData = tableData
        tempTableData[index].quantity = e.target.value
        setTableData(tempTableData)
        console.log(tableData, "tableleelelel")
    }

    function onSubmit() {
        const cartItems = tableData.filter((data) => {return data.quantity != null})
        const finalCartItems = [] 
        cartItems.map((item) => {finalCartItems.push({productId: Number(item.id), quantity: Number(item.quantity)})})
        console.log(finalCartItems, "cart tiems")
        setSubmitObject(
            {
                customerName: name, 
                customerEmail: email, 
                orderDescription: orderDescription, 
                cartItems: finalCartItems
            }
        )

        Axios.post('http://localhost:8080/api/placeOrder', submitObject)

    }
    
    console.log(submitObject, "submit ojechehchehf  ")

    return (
        
        <table className={styles.MainTable}>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Quantity to Add</th>
                    <th></th>
                </tr>
                
                    {tableData.map((rowData, index) => {
                        return(
                           // <>
                            <tr>
                                <td>{rowData.name}</td>
                                <td>{rowData.price}</td>
                                <td>{rowData.availableQuantity}</td>
                                <td>
                                    <input onChange={(e) => {onTypeNumber(e, index)}}/>
                                </td>
                                {/* <td><button>add to cart</button></td> */}
                            </tr>
                            //</>
                            )
                    })}
<br/>
<br/>
<br/>
<br/>
                    <div>--------------------------</div>

                    <div>Customer Details</div>
                    <div>name</div>
                    <div><input onChange={onNameChange}/></div>
                    <div>email</div>
                    <div><input onChange={onEmailChange}/></div>
                    <div>order description</div>
                    <div><input onChange={onDescriptionChange}/></div>

<br/>
<br/>
                    <div><button onClick={onSubmit}>submit order</button></div>
                
               
                
            </table>
        // <Table theadData={getHeadings()} tbodyData={res}> 
        //     <thead>
        //         <tr>
        //         {theadData.map(heading => {
        //             return <th key={heading}>{heading}</th>
        //         })}
    
        //         </tr>
        //     </thead>
        //     <tbody>
        //         {tbodyData.map((row, index) =>{
        //             return <tr key={index}>
        //                 {
        //                     theadData.map((key,index) =>
        //                     {
        //                         return <td key= { row[key] }>{row[key]} </td>
        //                     })}
    
        //             </tr>;
        //         })}

        //     </tbody>

        // </Table>
    );
        
    
    
            
}

export default Index;